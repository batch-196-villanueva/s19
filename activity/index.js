console.log("Hello World")

let username, password, role;

function login(){
	username = prompt("Enter username:");
	password = prompt("Enter password:");
	role = prompt("Enter role:").toLowerCase();

	if(username==="" || password === "" || role === ""){
		alert("Input must not be empty! Let's try that again.");
		login()
	} else {
		switch(role){
			case 'admin':
				alert("Welcome back to the class portal, admin!");
				break;
			case 'teacher':
				alert("Thank you for logging in, teacher!");
				break;
			case 'student':
				alert("Welcome to the class portal, student!");
				break;
			default:
				alert("Role out of range.")
		}
	}
}

login()

function checkAverage(a,b,c,d){
	let average = (a+b+c+d)/4;

	switch(true){
		case (Math.round(average) < 75):
			console.log("Hello, student. Your average is " + average + ". The letter equivalent is F");
			break;
		case (Math.round(average) < 80) :
			console.log("Hello, student. Your average is " + average + ". The letter equivalent is D");
			break;
		case (Math.round(average) < 85):
			console.log("Hello, student. Your average is " + average + ". The letter equivalent is C");
			break;
		case (Math.round(average) < 90):
			console.log("Hello, student. Your average is " + average + ". The letter equivalent is B");
			break;
		case (Math.round(average) < 96):
			console.log("Hello, student. Your average is " + average + ". The letter equivalent is A");
			break;
		case (Math.round(average) > 95):
			console.log("Hello, student. Your average is " + average + ". The letter equivalent is A+");
			break;

	}
}